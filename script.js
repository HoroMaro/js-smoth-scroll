(function(){

    'use strict'

    const navLinks = document.querySelectorAll('nav ul li a');

    navLinks.forEach((eachLink) => {
        eachLink.addEventListener('click', smoothScroll);
    })

    function smoothScroll(e) {
        e.preventDefault();

        const targetID = e.target.getAttribute('href');
        const targetSection = document.querySelector(targetID);
        const originalTop = Math.floor(targetSection.getBoundingClientRect().top) - 200;

        window.scrollBy({top: originalTop, left: 0, behavior: 'smooth'});
    }

    const posts = document.querySelectorAll('section');
    let postTops = [];
    let pageTop;
    let counter = 1;
    let prevCounter = 1;
    let doneResizing;

    resetPagePosition();

    // console.log(postTops);

    window.addEventListener('scroll', () => {
        pageTop = window.pageYOffset;

        if (pageTop > postTops[counter]) {
            counter++;
        } else if (counter > 1 && pageTop < postTops[counter - 1]) {
            counter--;
        }

        if (counter !== prevCounter) {
            navLinks.forEach((eachLink) => {
                eachLink.removeAttribute('class');
            });

            const thisLink = document.querySelector(`nav ul li:nth-child(${counter}) a`);
            thisLink.className = 'selected';
            prevCounter = counter;
        }
    });

    window.addEventListener('resize', function () {
        clearTimeout(doneResizing);

        doneResizing = setTimeout(function () {
            resetPagePosition();
        }, 500);
    });

    function resetPagePosition() {
        postTops = [];

        posts.forEach(function (eachPost) {
            postTops.push(Math.floor(eachPost.getBoundingClientRect().top + window.pageYOffset));
        });

        const pagePosition = window.pageYOffset + 250;
        counter = 0;

        postTops.forEach(function (eachPost) {
            if (pagePosition > eachPost) {
                counter++;
            }
        });

        navLinks.forEach((eachLink) => {
            eachLink.removeAttribute('class');
        });

        const thisLink = document.querySelector(`nav ul li:nth-child(${counter}) a`);
        thisLink.className = 'selected';
    }
})()